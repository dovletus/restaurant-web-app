const express = require("express");
const jsonServer = require("json-server");

const app = express();
const port = 4000;

app.use(jsonServer.defaults());
// app.use("/static", express.static("public"));
app.use(express.static("public"));

const router = jsonServer.router("db.json");
app.use(router);

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.listen(port, () => {
  console.log(`Starting json server server on port ${port}`);
});
