var jsonServer = require('json-server')
 
// Returns an Express server
var server = jsonServer.create()
 
const port = 4000
const host = 'localhost'
// const host = 'http://192.168.1.113'
// const host = '192.168.1.113'

// Set default middlewares (logger, static, cors and no-cache)
server.use(jsonServer.defaults())
 
// Add custom routes
// server.get('/custom', function (req, res) { res.json({ msg: 'hello' }) })
 
// Returns an Express router
var router = jsonServer.router('db.json')
server.use(router)
 
server.listen(port,host,()=>console.log('Starting server at', port))