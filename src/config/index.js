const modes = {
  development: true,
  production: false,
};

const hosts = {
  development: "http://localhost:4000",
  production: "https://your.server.com",
};

export const isDevEnv = modes.development; //production

export const baseUrl = isDevEnv ? hosts.development : hosts.production;

export const minPasswordLength = isDevEnv ? 1 : 8;
export const inputErrMsg = "Bu ýeri doly we dogry doldurmaly";

export const minTotalPrice = 12

export const shippingCost = 12

