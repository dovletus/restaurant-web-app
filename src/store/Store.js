import { makeAutoObservable } from "mobx";

export class Store {
    // list=[]
    // key1=null
    // key2=null

    constructor(stateObj){//stateObj={key1:{}, key2: []}
        const keys = Object.keys(stateObj)

        for(let key of keys){
            const value = stateObj[key]
            
            if(Object.prototype.toString.call(value)==='[object Object]') this[key]={...value}
            if(Object.prototype.toString.call(value)==='[object Array]') this[key]=[...value]
            else this[key] = value
        }

        makeAutoObservable(this)
    }

    // setKeys(keyList,valueList){ 
    setKeys(objList){ //[{key1: value1}, {key2: value2}]

        // //Method1: [key1,key2],[value1,value2] 
        // let i = 0
        // for(let key of keyList){
        //     this[key] = valueList[i]
        //     i++
        // }

        // Mehtod2: [{key1:value1},{key2,value2}]
        for(let item of objList){ //item = {key1: value1} 
            const key = Object.keys(item)[0]
            const value = item[key]
            this[key] = value
        }

    }
    
    getKeys(keys){ // ['key1', 'key2'] => {key1: value1, key2: value2}
        const result = {}

        for(let key of keys){            
            result[key] = this[key] //result.key1 = value1 //result.key2 = value2 
        }
        
        return result; //result = {key1: value1, key2: value2}
    }

    getById(key,id){ // key = []
        return this[key].find(item=>item.id===id)
    }

    getBySelector(key, selector){
        return this[key].filter(item=>selector(item))
    }

}

// value={{
//     user: new Store(userState),
//     categories: new Store(categoryState),
//     products:  new Store(productState)
// }}