import { makeAutoObservable } from "mobx";
import { makePersistable } from "mobx-persist-store";

export class LikeStore {
    list=[]
    idList=[]

    constructor(){
        makeAutoObservable(this)

        makePersistable(this, {
            name:'_LikeStore',
            properties: [
                {
                  key: 'list',
                  serialize: (value) => JSON.stringify(value)
                //   {
                //     return value.join(',');
                //   }
                  ,
                  deserialize: (value) => JSON.parse(value)
                //   {
                //     return value.split(',');
                //   }
                  ,
                },
                {
                    key: 'idList',
                    serialize: (value) => 
                    {
                      return value.join(',');
                    }
                    ,
                    deserialize: (value) => 
                    {
                      return value ? value.split(',').map(Number) : []
                    }
                    ,
                  },
              ],
            storage: window.localStorage
        })
    }

    setList(list){
        this.list= [...list]
        this.idList = list.map(item=>item.id)
    }

    get List(){
        return this.list
    }

    addToLike (product){
        const isPresent = this.idList.includes(product.id)
        if (isPresent) return;

        this.list.push({...product})
        this.idList.push(product.id)
    }

    removeFromLike (id){
        this.list = this.list.filter(item=>item.id!==id)
        this.idList = this.idList.filter(element=>element!==id)
    }

    emptyLike(){
        this.list=[]
        this.idList=[]
    }

    isLikeProduct(id){
        return this.idList.includes(id)
    }

    get LikeListLength(){
        return this.idList.length
    }
}