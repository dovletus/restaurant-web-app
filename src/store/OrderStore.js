import { makeAutoObservable } from "mobx";
import { makePersistable } from "mobx-persist-store";

export class OrderStore {
    list=[]
    idList=[]
    selected={}

    constructor(){
        makeAutoObservable(this)

        makePersistable(this, {
            name:'_OrderStore',
            properties: [
                {
                  key: 'list',
                  serialize: (value) => JSON.stringify(value),
                  deserialize: (value) => JSON.parse(value),
                },
                {
                    key: 'idList',
                    serialize: (value) => value.join(','),
                    deserialize: (value) => value ? value.split(',').map(Number) : [],
                  },
              ],
            storage: window.localStorage
        })
    }

    setList(list){
        this.list= [...list]
        this.idList = list.map(item=>item.id)
    }

    get List(){
        return this.list
    }

    addToOrders (order){
        const isPresent = this.idList.includes(order.id)
        if (isPresent) return;

        this.list.push({...order})
        this.idList.push(order.id)
    }

    removeFromOrders (id){
        this.list = this.list.filter(item=>item.id!==id)
        this.idList = this.idList.filter(element=>element!==id)
    }

    emptyOrders(){
        this.list=[]
        this.idList=[]
    }

    get OrderListLength(){
        return this.idList.length
    }

    setSelected(order){
      this.selected = order
    }

    get SelectedOrder(){
      return this.selected
    }
}