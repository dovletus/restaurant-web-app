import { makeAutoObservable } from "mobx";
import { makePersistable } from "mobx-persist-store";

export class BasketStore {
    list=[]
    idList=[]
    totalPrice=0
    //    

    constructor(){
        makeAutoObservable(this)

        makePersistable(this, {
            name:'_BasketStore',
            properties: [
                {
                  key: 'list',
                  serialize: (value) => JSON.stringify(value),
                  deserialize: (value) => JSON.parse(value),
                },
                {
                    key: 'idList',
                    serialize: (value) =>value.join(','),
                    deserialize: (value) => value ? value.split(',').map(Number) : []
                },
                  ,
                  {
                    key: 'totalPrice',
                    serialize: (value) =>value.toString(),
                    deserialize: (value) => Number(value),
                  },
              ],
            storage: window.localStorage
        })
    }

    setList(list){
        this.list= [...list]
        this.idList = list.map(item=>item.id)
    }

    get List(){
        return [...this.list]
    }

    addToBasket (product){
        // const isPresent = this.idList.includes(product.id)
        const isPresent = this.list.find(item=>item.id===product.id)

        if (isPresent) return;

        this.list.push({...product, qty:1, totalPrice: product.price})
        this.idList.push(product.id)
        this.totalPrice+=product.price
    }

    removeFromBasket (id){
        const product = this.list.find(item=>item.id===id)

        this.list = this.list.filter(item=>item.id!==id)
        this.idList = this.idList.filter(element=>element!==id)

        this.totalPrice -=product.totalPrice
    }

    updateBasketProduct(product){

        const oldProduct = this.list.find(item=>item.id===product.id)

        const totalChange = product.totalPrice - oldProduct.totalPrice

        this.list = this.list.map(item=>item.id===product.id ? {...product} : {...item})

        this.totalPrice += totalChange

    }

    emptyBasket(){
        this.list=[]
        this.idList=[]
        this.totalPrice = 0
    }

    isBasketProduct(id){
        return this.idList.includes(id)
    }

    get BasketListLength(){
        return this.idList.length
    }

    get TotalPrice(){
        return this.totalPrice
    }
}