import { makeAutoObservable } from "mobx";

export class CategoryStore {
  list = []; //[{id,title,img}]
  selectedItem = {}; // {category}

  constructor() {
    makeAutoObservable(this);
  }

  setSelected(cat) {
    this.selectedItem = { ...cat };
  }

  get SelectedItem() {
    return this.selectedItem;
  }

  setList(list) {
    this.list = [...list];
    this.selectedItem =  (list.length)? {...list[0]} : null
  }

  addItem(category) {
    this.list.push(category);
  }

  getItem(id) {
    return this.list.find((item) => item.id === id);
  }

  removeItem(id) {
    this.list = this.list.filter((item) => item.id !== id);
  }

  get List() {
    return this.list;
  }
}
