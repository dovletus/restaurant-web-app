import { makeAutoObservable } from "mobx";

const initialState = [
  {
    id: 0,
    cat: 0,
    title: "product 1",
    description: "description of product 1",
    price: 1,
    type: "auction,new",
    img: "1.jpg",
  },
  {
    id: 1,
    cat: 1,
    title: "product 2",
    description: "description of product 2",
    price: 2.5,
    type: "auction",
    img: "2.jpg",
  },
  {
    title: "product 3",
    description: "descrition 3",
    price: "12.5",
    type: "new",
    id: 2,
    img: "3.jpg",
  },
];

export class ProductStore {
  constructor() {
    makeAutoObservable(this);
  }

  list = [...initialState];
  title = "";
  selectedId = null;
  isUpdate = false;

  setTitle(title) {
    this.title = title;
  }
  
  get getTitle() {
    return this.title;
  }

  setSelectedId(id) {
    this.selectedId = id;
  }

  setIsUpdate(isUpdate) {
    this.isUpdate = isUpdate;
  }

  addOne(product) {
    const found = this.list.find((item) => item.id === product.id);
    if (!found) this.list.push(product);
  }

  addList(list) {
    this.list = [...this.list, ...list];
  }

  update() {
    const updatedProduct = {
      id: this.selectedId,
      title: this.title,
    };
    this.list = this.list.map((item) =>
      item.id === this.selectedId ? updatedProduct : item
    );
  }

  deleteOne(id) {
    this.list = this.list.filter((item) => item.id !== id);
  }

  deleteAll() {
    this.list = [];
  }

  get getAll() {
    return this.list;
  }

  getOne(id) {
    return this.list.find((item) => item.id === id);
  }
}
