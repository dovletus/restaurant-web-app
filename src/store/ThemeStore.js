import { makeAutoObservable } from "mobx";

const colors = {
  dark: "black",
  light: "white",
};

export class ThemeStore {
  isDark = false;
  textColor = "";
  bgColor = "";

  constructor() {
    makeAutoObservable(this);

    this.setColors();
  }

  //method
  setIsDark(boolValue) {
    this.isDark = boolValue;
    this.setColors();    
  }

  toggleTheme() {
    this.isDark = !this.isDark;
    this.setColors();

  }

  get IsDark() {
    return this.isDark;
  }

  //attribute
  get themeColors() {
    return { bgColor: this.bgColor, textColor: this.textColor };
  }

  setColors() {
    this.textColor = this.isDark ? colors.light : colors.dark;
    this.bgColor = this.isDark ? colors.dark : colors.light; //colors[boolValue ? "dark" : "light"];
  }
}
