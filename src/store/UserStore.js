import { makeAutoObservable } from "mobx";
import { makePersistable } from "mobx-persist-store";

export const userStoreKeys={
  user:"user",
  isAuth:"isAuth",
  token:"token",
  isFormVisible:"isFormVisible",
  loading:"loading",
  fetched:"fetched"
}

export class UserStore{
    user={}//{id,name,level,avatar}
    isAuth=false
    token=null
    isFormVisible=false;
    loading = false
    fetched = false

    constructor(){
        makeAutoObservable(this)

        makePersistable(this, {
            name:'_UserStore',
            properties: [
                {
                  key: 'user',
                  serialize: (value) => JSON.stringify(value),
                  deserialize: (value) => JSON.parse(value),
                },
                  {
                    key: 'isAuth',
                    serialize: (value) =>value,
                    deserialize: (value) => Boolean(value),
                  },
                  {
                    key: 'token',
                    serialize: (value) =>value,
                    deserialize: (value) => value,
                  },
              ],
            storage: window.localStorage
        })
    }

    login(data){
      const {name,email,password, token}=data
        this.user = {name,email,password}
        this.token = token
        this.isAuth = true
    }

    logout() {
        this.user={}
        this.isAuth = false
    }

    get User(){
        return this.user
    }

    get IsAuth(){
        return this.isAuth
    }

    setLoginFormVisible(bool){
      this.isFormVisible = bool
    }

    get LoginFormVisible(){
      return this.isFormVisible
    }

    getKey(key){
      if(!userStoreKeys[key]) return;

      return this[key]
    }

    setKey(key,value){
      if(!userStoreKeys[key]) return;

      // this[key]=typeof value ==='object' ? {...value} : value

      if(Object.prototype.toString.call(value) === "[object Object]") this[key]= {...value}
      else if (Object.prototype.toString.call(value) === "[object Array]") this[key]= [...value]
      else this[key]=value
    }
}