import { makeAutoObservable } from "mobx";
import { baseUrl } from "../config";
import { host } from "../http";

export class ProductStore {
  list = []; //[{id,title,img}]
  selectedItem = {}; // {category}
  updateItem = null;

  constructor() {
    makeAutoObservable(this);
    //make persistable

  }

  setSelected(item) {
    this.selectedItem = { ...item };
  }

  get SelectedItem() {
    return this.selectedItem;
  }

  setList(list) {
    this.list = [...list];
    this.selectedItem = (list.length) ? {...list[0]} : null
  }

  addItem(item) {
    this.list.push(item);
  }

  getItem(id) {
    return this.list.find((item) => item.id === id);
  }

  removeItem(id) {
    this.list = this.list.filter((item) => item.id !== id);
  }

  get List() {
    return this.list;
  }

  updateRating(id,userRating){
    const stateObj = this.list.find(item=>item.id===id)
    const product = {...stateObj}

    product.rating = (product.ratersQty * product.rating + userRating)/(product.ratersQty+1)

    product.ratersQty++

    
    host.put(`${baseUrl}/products/${id}`,product)
    .then(res=>{
      if(res.status===200) {
        this.list = this.list.map(item=>item.id===product.id ? product : item)
        
      } else {
        console.log('fetch result:',res.data)
      }
    })
    .catch(err=>console.log(err))
  }

  setUpdateItem(product){
    this.updateItem = product
  }

  get UpdateItem(){
    return this.updateItem
  }

  updateProduct(product){
    this.list = this.list.map(item=>item.id===product.id ? product : item)
  }
}
