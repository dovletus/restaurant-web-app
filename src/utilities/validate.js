import { minPasswordLength } from "../config";

export function validateName(str) {
  var regex = /^[a-zA-Z ]{2,30}$/;

  return regex.test(str);
}

export function validateEmail(str) {
  var regex =
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

  return regex.test(str);
}

export function validatePassword(str) {
  return str.replaceAll(" ", "").length >= minPasswordLength;
}

export function validateTel(str) {
  // var regex = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
  // return regex.test(str);

  let temp = str[0] === "+" ? str.replace("+", "") : str;
  temp = Number(temp);
  
  return Number.isInteger(temp) && temp > 0;
}
