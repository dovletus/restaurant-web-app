import { isDevEnv } from "../config";


class DevLog {
    log(...args){
        if(isDevEnv) console.log('>>>',...args)
    }

    warn(...args){
        if(isDevEnv) console.log('! Warning:'.padEnd(15),...args)
    }

    error(...args){
        if(isDevEnv) console.log('? Error:'.padEnd(15),...args)
    }

    finally(...args){
        if(isDevEnv) console.log('v Finally:'.padEnd(15),...args)
    }
}

export const dlog = new DevLog()