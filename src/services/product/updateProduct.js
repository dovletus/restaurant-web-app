import { host } from "../../http";
import { dlog } from "../../utilities/log";

export async function updateProduct(product){
    return await host.put(`/products/${product.id}`,product)
        .then(res=>res.data)
        .catch(err=>{
            dlog.error(`update product (id:${product.id})`,err)
            return null;
        })
    
}