import { host } from "../../http";
import { dlog } from "../../utilities/log";

export async function deleteProduct(id){
    return await host.delete(`/products/${id}`)
        .then(res=>res.data)
        .catch(err=>{
            dlog.error(`delete product (id:${id})`,err)
            return null;
        })
    
}