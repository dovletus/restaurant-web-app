import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'
import { useNavigate } from 'react-router-dom'

import { MyContext } from '../../App'
import { routes } from '../../navigation/routes'
import styles from './styles.module.css'

const ProductDetailsPage=observer(()=> {
  const {productStore} = useContext(MyContext)

  const navigate = useNavigate()
  
  const product = productStore.SelectedItem

  const handleNav=()=>navigate(routes.main)

  return (
    <div>
      <button onClick={handleNav} >Main page</button>
      
    <div className={styles.root}>
      <span className={styles.header}>Onum barada</span>
      <span className={styles.title}>{product.title}</span>
      <span className={styles.description}>{product.description}</span>
      <span className={styles.price}>{product.price}</span>
      </div>
    </div>
  )
})

export default ProductDetailsPage

// import React from 'react'
// import styles from './styles.module.css'

// export default function ProductDetailsCard({product}) {
//   console.log('product',product)
//   if (product) 
//   return (
    
//     <div className={styles.root}>
//     <span className={styles.header}>Onum barada</span>
//     <span className={styles.title}>{product.title}</span>
//     <span className={styles.description}>{product.description}</span>
//     <span className={styles.price}>{product.price}</span>
//   </div>
//   )
//   return null
// }