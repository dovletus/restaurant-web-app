import LikePage from "./LikePage";
import BasketPage from "./BasketPage";
import MainPage from "../components/MainPage";
import ProductDetailsPage from "./ProductDetailsPage";
import OrderPage from "./OrderPage";
import OrderDetails from "./OrderDetails";
import AdminPage from "./AdminPage";
import AdminProductsPage from "./AdminPage/adminProductsPage";
import AdminCategoriesPage from "./AdminPage/adminCategoriesPage";

export {
    MainPage,
    ProductDetailsPage,
    LikePage,
    BasketPage,
    OrderPage,
    OrderDetails,
    AdminPage,
    AdminCategoriesPage,
    AdminProductsPage
}