import { faLeftLong, faLeftRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
// import { useParams, useRoutes } from 'react-router-dom'
import { MyContext } from '../../App'
import ProductCard from '../../components/product/ProductCard'
import ProductQty from '../../components/product/ProductQty'
import { routes } from '../../navigation/routes'
import  styles from './styles.module.scss'

export default function OrderDetails() {
  const {orderStore} = useContext(MyContext)

    const {id,  totalSum, productList=[]} = orderStore.SelectedOrder;// || {}
    let date = orderStore.SelectedOrder
    date = new Date(date.date).toLocaleString("tr-TR", { timeZone: "Asia/Ashgabat" })  
    return (
    <div className={styles.root}>
      <Link to={routes.order}><FontAwesomeIcon icon={faLeftLong} style={{color: 'seagreen'}}/></Link>
      
      <div className={styles.header}>
        <span>ID: {id}</span>
        <span className={styles.date}>Date: {date}</span>
        <span>Total sum: {totalSum}</span>

      </div>
      <div className={styles.list}>
        {productList.map(product=>(
          <ProductCard isInOrderDetails={true} product={product}>
            <ProductQty qty={product.qty}/>
          </ProductCard>
        ))}
      </div>
    </div>
  )
}
