import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { useNavigate } from 'react-router-dom'

import { MyContext } from '../../App'
import ProductCard from '../../components/product/ProductCard'
import { routes } from '../../navigation/routes'
import styles from './styles.module.css'

const LikePage=observer(()=> {
  const {likeStore}=useContext(MyContext)

  const list =likeStore.list

  let navigate = useNavigate()

  const handleNav=()=>navigate(routes.main)

  const handleClearLikeList = ()=>likeStore.emptyLike()

  return (
    <div>
      <button onClick={handleNav} >Main page</button>
      <p>Halanlarym</p>
      <button onClick={handleClearLikeList}>Clear</button>


      <div className={styles.container}>
        {list.map((item) => (
          <ProductCard product={item} key={item.id} />
        ))}
      </div>
    </div>
  )
})

export default LikePage