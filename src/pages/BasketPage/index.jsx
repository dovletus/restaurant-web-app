import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { useNavigate } from 'react-router-dom'

import { MyContext } from '../../App'
import { ChangeQuantity, TotalPriceComponent } from '../../components/pageBasket'
import ProductCard from '../../components/product/ProductCard'
import { routes } from '../../navigation/routes'
import styles from './styles.module.css'

const BasketPage=observer(()=> {
  const {basketStore}=useContext(MyContext)

  const list = [...basketStore.list]

  let navigate = useNavigate()

  const handleNav=()=>navigate(routes.main)

  const handleClearBasket = ()=>basketStore.emptyBasket()

  return (
    <div>
      <button onClick={handleNav} >Main page</button>
      <p>Sebet</p>
      {list.length>0 && 
      <>

      <button onClick={handleClearBasket}>Clear</button>

      <div className={styles.container}>
        {list.map((item) => (
          <ProductCard product={item} key={item.id} >
            <ChangeQuantity product={{...item}}/>
          </ProductCard>
        ))}
      </div>
      <TotalPriceComponent />
        </>      }
    </div>
  )
})

export default BasketPage