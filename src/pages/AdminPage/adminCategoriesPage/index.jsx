import React, { useState, useEffect } from 'react'

import { baseUrl } from '../../../config'
import { fetchlist } from '../../../http/fetchList'
import { AdminNavbar } from '../components'
import  './styles.scss'

export default function AdminCategoriesPage() {
const [list,setList]=useState([])


useEffect(()=>{
  (async function(){
    const newList = await fetchlist(`/categories`)

    if(newList) setList(newList)
  }

  )()
},[])

  return (
    <div>
      <AdminNavbar/>
      <h2>Categories</h2>
      <div>
      <table>
            <thead>
              <tr>
                <td>No</td>
                <td>Category name</td>
                <td>Image</td>
                <td>Action</td>              
              </tr>
            </thead>
            <tbody>
              {list.map((item,i) => (
                <tr key={item.id}>
                    <td>{i+1}</td>
                    <td>{item.title}</td>
                    <td><img src={`${baseUrl}/images/${item.img}`} alt="" /></td>
                    <td>Upd-Del</td>
                </tr>
                ))}
            </tbody>
          </table>
      </div>
    </div>
  )
}
