import { observer } from 'mobx-react-lite'
import React, {useState, useEffect, useContext} from 'react'
import { MyContext } from '../../../App'

import {fetchlist} from '../../../http/fetchList'
import { AdminNavbar, Modal, UpdateProductForm } from '../components'
import { AdminProductRow } from '../components/adminProducts/AdminProductRow'
import './styles.scss'

 const AdminProductsPage=observer(()=> {
  // const [list,setList]=useState([])
  const {productStore}=useContext(MyContext)
  
  const list=productStore.List

  const updateItem = productStore.UpdateItem

  // useEffect(()=>{
  //   (async function(){
  //     if (list.length) return;

  //     const newList = await fetchlist(`/products`)
  
  //     productStore.setList(newList)
  //   }
  
  //   )()
  // },[])
  
    return (
      <div>
        <AdminNavbar/>
        <h2>Products</h2>
        <div className='container'>
        <table>
              <thead>
                <tr>
                  <td>No</td>
                  <td>Category</td>
                  <td>Product name</td>
                  <td>Price</td>
                  <td>Discount</td>
                  <td>Old price</td>
                  <td>Type</td>
                  <td>Image</td>
                  <td>Description</td>
                  <td>Action</td>              
                </tr>
              </thead>
              <tbody>
                {list.map((item,i) => (
                  <AdminProductRow product={item} i={i+1} key={item.id}/>
                  ))}
              </tbody>
            </table>
        </div>
        {
          updateItem && (
            <Modal>
              <UpdateProductForm />
            </Modal>
          )
        }
      </div>
    )
})

export default AdminProductsPage