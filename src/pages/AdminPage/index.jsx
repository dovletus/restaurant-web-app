import React from 'react'

import {AdminNavbar} from './components'

export default function AdminPage() {
  return (
    <div >
      <AdminNavbar />
    </div>
  )
}
