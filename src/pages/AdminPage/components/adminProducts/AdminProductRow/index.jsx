import { faPencil, faRemove } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { MyContext } from '../../../../../App'
import { baseUrl } from '../../../../../config'
import { deleteProduct } from '../../../../../services'
import styles from './styles.module.scss'

export const AdminProductRow=observer(({product,i})=> {
const {categoryStore, productStore}=useContext(MyContext)

const category = categoryStore.getItem(product.catId)

    function handleUpdate() {
      productStore.setUpdateItem(product)      
    }
       
       function handleDelete() {
         const answer = confirm('Are you sure that you permanently want to delete this product?')
         if(answer){
            productStore.removeItem(product.id)
            deleteProduct(product.id)
         }
        
        }
  return (
    <tr>
        <td>{i}</td>
        <td>{category.title}</td>
        <td>{product.title}</td>
        <td>{product.price}</td>
        <td>{product.discount}</td>
        <td>{product.oldPrice}</td>
        <td>{product.type}</td>
        <td><img src={`${baseUrl}/images/${product.img}`} alt="" className={styles.img}/></td>
        <td>{product.description}</td>
        <td>
        <div className="btns">            
            <FontAwesomeIcon icon={faPencil} color='green' onClick={handleUpdate} className={styles.btn}/>
            <FontAwesomeIcon icon={faRemove} color='red' onClick={handleDelete} className={styles.btn}/>
        </div>
        </td> 
    </tr>
  )
})

 