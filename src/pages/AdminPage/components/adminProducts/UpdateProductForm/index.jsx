import React, { useContext, useState } from 'react'
import { faSave } from '@fortawesome/free-regular-svg-icons'
import { faCancel } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { MyContext } from '../../../../../App'
import { updateProduct } from '../../../../../services'

import styles from './styles.module.scss'

export function UpdateProductForm() {
    const {productStore}=useContext(MyContext)

    const item = productStore.UpdateItem
    const [product,setProduct]=useState(item ? {...item} : {})

    function handleChangeProduct(key,value){
        setProduct(prev=>({...prev, [key]:value}))
        
        // const newProduct = {...product, [key]:value}
        // setProduct(newProduct)
    }

    function handleSave(){
        productStore.updateProduct(product)
        updateProduct(product)

        productStore.setUpdateItem(null)        
    }

    const handleCancel = ()=> productStore.setUpdateItem(null)        

  return (
    <div className={styles.root}>
        <table>
            <tr>
                <td>
                    <span>Title</span>
                    <input type="text" value={product.title}
                        onChange={e=>handleChangeProduct('title', e.target.value)}/>
                </td>
                <td>
                    <span>Price</span>
                    <input type="number" value={product.price}
                        onChange={e=>handleChangeProduct('price', e.target.value)}/>
                </td>
            </tr>
                <tr>

                <td>
                    <span>Discount</span>
                    <input type="number" value={product.discount}
                        onChange={e=>handleChangeProduct('discount', e.target.value)}/>
                </td>
                <td>
                    <span>Old Price</span>
                    <input type="number" value={product.oldPrice}
                        onChange={e=>handleChangeProduct('oldPrice', e.target.value)}/>
                </td>
            </tr>
            <tr>
                <td>
                    <span>Type</span>
                    <input type="text" value={product.type}
                        onChange={e=>handleChangeProduct('type', e.target.value)}/>
                </td>
                <td>
                    <span>Description</span>
                    <input type="text" value={product.description}
                        onChange={e=>handleChangeProduct('description', e.target.value)}/>
                </td>
            </tr>
            <tr>
                <td>
                    <FontAwesomeIcon icon={faSave} color='green' onClick={handleSave}/>
                </td>
                <td>
                    <FontAwesomeIcon icon={faCancel} color='gray' onClick={handleCancel}/>
                </td>
            </tr>
        </table>
    </div>
  )
}
