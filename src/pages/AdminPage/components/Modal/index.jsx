import React from 'react'
import styles from './styles.module.scss'

export function Modal({children}) {
  return (
    <div className={styles.root}>
        <div className={styles.modalView}>
          {children}
        </div>
    </div>
  )
}
