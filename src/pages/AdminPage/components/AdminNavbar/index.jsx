import React, { useContext, useEffect, useState } from 'react'
import { Link, useLocation } from 'react-router-dom'

import { routes } from '../../../../navigation/routes'
import Logo from '../../../../components/Logo'
import styles from './styles.module.scss'
import { observer } from 'mobx-react-lite'
import { MyContext } from '../../../../App'
import { Modal } from '../Modal'
import { LoginForm } from '../../../../components'

const links = [
    {route:routes.main, title: "Main page"},
    {route:routes.admin, title: "Admin page"},
    {route:routes.adminCategories, title: "Categories"},
    {route:routes.adminProducts, title: "Products"},
]

export  const AdminNavbar=observer(()=> {
  const {pathname} = useLocation()
  const {userStore} = useContext(MyContext)

  const [innerWidth, setInnerWidth]=useState(100)

  const isAuth = userStore.IsAuth
  const isLoginFormVisible = userStore.LoginFormVisible

  function handleLogin(){
    if(isAuth){
      userStore.logout()
    } else {
      userStore.setLoginFormVisible(true)
      console.log('press on Log in')
      // userStore.login({id:1, name:'Admin user', level:'ADMIN', token: 'abc123'})
    }
  }

    let myLinks = links.filter(link=>link.route!==pathname)
    // myLinks = links.filter(link=>link.route!==pathname)

    useEffect(()=>{
      setInnerWidth(window.innerWidth-20);
    },[])

  return (
    <>
    <div className={styles.root}>
      <Logo />
      {isAuth &&
      <div className={styles.linkContainer}>
        {
          // myLinks.map(item=>item.link)
          myLinks.map(item=>(
            <Link to={item.route} className={styles.link} key={item.route}>{item.title}</Link>
            ))
          }
        </div>
}
        <div className={styles.empty}/>
        {!isLoginFormVisible &&
        <div className={styles.login}>
          <span className={styles.loginBtn} onClick={handleLogin}>{isAuth ? 'Log out' : 'Log in'}</span>
        </div>
        } 
    </div>
    {isLoginFormVisible &&
      <Modal>
      <LoginForm />
    </Modal>
    }
      {/* <svg height="10" width={innerWidth} style={{marginTop:10}}>
        <line x1="0" y1="0" x2={innerWidth} y2="0" className={styles.gutter} />
        Sorry, your browser does not support inline SVG.
      </svg> */}

      </>
  )
})

