import React, { useContext,useEffect } from 'react'
import { observer } from 'mobx-react-lite'
import { useNavigate } from 'react-router-dom'

import { MyContext } from '../../App'
import { routes } from '../../navigation/routes'
import styles from './styles.module.scss'
import { host } from '../../http'
import OrderCard from '../../components/pageOrder/OrderCard'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLeftLong } from '@fortawesome/free-solid-svg-icons'

const OrderPage=observer(()=> {
  const {orderStore}=useContext(MyContext)

  const list =orderStore.list

  let navigate = useNavigate()

  const handleNav=()=>navigate(routes.main)

  const handleClearOrderList = ()=>{
    list.forEach(order=>{
      host.delete(`/orders/${order.id}`)
    })
    orderStore.emptyOrders()}

  useEffect(()=>{
    async function fetchOrders(){
      host.get(`/orders`)
      .then(({data})=> orderStore.setList(data))
      .catch(err =>dlog.error('Error in fetchin orders..', err))
    }

    if (list.length===0) fetchOrders()
  },[])

  return (
    <div>
      <FontAwesomeIcon icon={faLeftLong} style={{color: 'seagreen', userSelect: 'none', cursor: "pointer"}} onClick={handleNav}/>
      <p>Sargytlar</p>
        {list.length>0 && 
        <>
      <button onClick={handleClearOrderList}>Clear</button>

      <div className={styles.container}>
          <table>
            <thead>
              <tr>
                <th>No</th>
                <th>Date</th>
                <th>Total sum</th>
                <th>Action</th>              
              </tr>
            </thead>
            <tbody>
              {list.map((item,i) => (
                <OrderCard order={item} index={i+1} key={item.id} />
                ))}
            </tbody>
          </table>
      </div>
          </>  }
        
    </div>
  )
})

export default OrderPage

