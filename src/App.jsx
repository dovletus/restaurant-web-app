import React, { createContext } from "react";

import "./App.css";
import "./assets/fonts/Poppins-Regular.ttf"
import "./assets/fonts/Poppins-Medium.ttf"
import "./assets/fonts/Poppins-SemiBold.ttf"

import { CategoryStore, ProductStore, ThemeStore,LikeStore, BasketStore, OrderStore, UserStore, Store } from "./store";
import Navigation from "./navigation";
import { testModel } from "./models";
import Card from "./components/xTest/Card";
import NumberCard from "./components/xTest/NumberCard";
import NumberContextProvider from "./hooks/numberContext";

export const MyContext = createContext();

export default function App() {
  console.log('Render App')
  return (
    // <MyContext.Provider
    //   value={{
    //     themeStore: new ThemeStore(),
    //     productStore: new ProductStore(),
    //     categoryStore: new CategoryStore(),
    //     likeStore: new LikeStore(),
    //     basketStore: new BasketStore(),
    //     orderStore: new OrderStore(),
    //     userStore: new UserStore(),
    //     testStore: new Store(testModel)
    //   }}
    // >
    //   <Navigation/>
    // </MyContext.Provider>

    <div>
      {/* <Card/>
      <br />
      <Card/> */}
      <NumberContextProvider>
            <NumberCard/>
            <br />
        
            <NumberCard/>
      </NumberContextProvider>

    </div>
  );
}
