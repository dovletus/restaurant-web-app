import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { MyContext } from '../../App'

export const WriteComponent=observer(()=> {
    const {testStore} = useContext(MyContext)

    function handleSet(){
        const list = [1,2,3,4,5]
        const loading =true

        testStore.setKeys([{list},{loading}])
    }

  return (
    <div>
        <button onClick={handleSet}>Set</button>
    </div>
  )
})
