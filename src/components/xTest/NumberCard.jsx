import React ,{useRef} from 'react'
import { useNumber } from '../../hooks'
import { useNumberCtx } from '../../hooks/numberContext'

export default function NumberCard() {
    // const [number, setNumber, increment, decrement]=useNumber(5)
    const {number, setNumberVal, increment, decrement}=useNumberCtx()

const changeByRef=useRef(1)

function handleIncrement(){
  increment(changeByRef.current)
}

function handleDecrement(){
  decrement(changeByRef.current)
}

function handleSetNumber(){
  setNumberVal(changeByRef.current)
}


return (
    <div>
        <p>Number {number}</p>
        <button onClick={handleIncrement}>Increment by</button>
        <button onClick={handleDecrement}>Decrement by</button>
        <button onClick={handleSetNumber}>Set number to</button>
        <input type="number" ref={changeByRef} defaultValue={1}/>


    </div>
  )
}
