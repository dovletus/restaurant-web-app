import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { MyContext } from '../../App'
import { testModelKeys } from '../../models'

export const ReadComponent=observer(()=> {
    const {testStore} = useContext(MyContext)

    const {list, loading} = testStore.getKeys([testModelKeys.list, testModelKeys.loading]) //[]=>{}

    // const selector = (item) => item.id===product.id ? true : false

    // const result =testStore.getBySelector(testModelKeys.list, selector)
    

  return (
    <div>
        {
        list.map(item=>(
            <p key={item}>{item}</p>
        ))
        }
            <h6>
            {
                loading ? 'Loading..' : 'Done loading.'
            }
            </h6>
    </div>
  )
})
