import React from 'react'
import { useToggle } from '../../hooks'

export default function Card() {
    const [value, toggle]=useToggle(false)

  return (
    <div>
        <p>Value {String(value)}</p>
        <button onClick={()=>toggle()}>Toggle</button>
    </div>
  )
}
