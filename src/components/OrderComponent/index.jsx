import React, { useContext, useEffect, useState } from 'react'
import { MyContext } from '../../App'
import { dlog } from '../../utilities/log'
import OrderCard from '../pageOrder/OrderCard'

export default function OrderComponent() {
    const [list, setList] = useState([])
    const {basketStore } = useContext(MyContext)
    const totalPrice = basketStore.totalPrice

    useEffect(()=> {
        async function fetchList(){
            host.get ("/orders")
            .then(({data}) => setList(data))
            .catch(err => dlog.error("Error in fetching"))
        }
        fetchList()
    },[])
    
  return (
    <div>
        {list.map((item)=> {
            <OrderCard  product={item} key={item.id}/>
        })}
    <span>TotalPrice: {totalPrice}</span>
    </div>
  )
}
