import React from 'react'
import styles from './styles.module.scss'

export default function Logo() {
  return (
    <div className={styles.root}>
        <span className={styles.title}>Logo</span>
    </div>
  )
}
