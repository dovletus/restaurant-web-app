import React from 'react'
import styles from './styles.module.css'

export default function ProductDetailsCard({product}) {
  
  if (product) 
  return (
    
    <div className={styles.root}>
    <span className={styles.header}>Onum barada</span>
    <span className={styles.title}>{product.title}</span>
    <span className={styles.description}>{product.description}</span>
    <span className={styles.price}>{product.price}</span>
  </div>
  )
  return null
}
