
import { observer } from "mobx-react-lite";
import React, { useContext } from "react";
import { useMemo } from "react";
import { useNavigate } from "react-router-dom";

import { MyContext } from "../../../App";
import { baseUrl } from "../../../config";
import { routes } from "../../../navigation/routes";
import ActionButtons from "../ActionButtons";
import PriceComponent from "../PriceComponent";
import RatingComponent from "../RatingComponent";
import styles from "./styles.module.css";

const ProductCard = observer(({ product, isInOrderDetails=false, children }) => {  
  const { productStore } = useContext(MyContext);
  const navigate = useNavigate()

  const selectedItem = productStore.SelectedItem;
 
  const isSelected = product.id === selectedItem.id;

  const bgColor = useMemo(
    () => ({
      backgroundColor: isSelected ? "white" : "white",
    }),
    [isSelected]
  );

  const handleClick = () => {
    productStore.setSelected(product);
    navigate(routes.productDetails)
  }

  return (
    <div className={styles.root} style={bgColor} >
      <div className={styles.imgContainer}>
        <img
          // src={{uri: `https://server/images/${product.img}`}}
          // src={imgSrc}
          src={`${baseUrl}/images/${product.img}`}
          className={styles.img}
          alt="slice of pizza "
          onClick={handleClick}
        />
      </div>
      <span className={styles.title}>{product.title}</span>
      <RatingComponent id={product.id} rating={product.rating} ratersQty={product.ratersQty}/>
      <PriceComponent value={{price:product.price, old_price:product.old_price, discount:product.discount}}/>
      {!isInOrderDetails && 
        <ActionButtons product={product}/>
      }
      {children}
    </div>
  );
});

export default ProductCard;
