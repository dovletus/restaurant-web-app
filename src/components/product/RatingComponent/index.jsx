import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { MyContext } from '../../../App'
import styles from './styles.module.css'

const starLength =5

const RatingComponent=observer(({id, rating,ratersQty})=> {
    const {productStore}=useContext(MyContext)
  
    const fullStarLength=Math.round(rating)
    const emptyStarLength=starLength- fullStarLength

    const starList=[] //[1,1,1,1,0]
    for(let i=1; i<=fullStarLength; i++) {starList.push(1)}

    if (emptyStarLength>0){

        for(let i=1; i<=emptyStarLength; i++) {starList.push(0)}
    }
    
    function handleGiveRating(index){
        productStore.updateRating(id,index+1)
    }

  return (
    <div className={styles.root}>
{
    starList.map((item,i)=>(
        item 
        ? <span key={`full star ${i}`} 
        className={styles.fullStar} onClick={()=>handleGiveRating(i)}>⭐</span>
        : <span key={`empty star ${i}`}
        className={styles.emptyStar} onClick={()=>handleGiveRating(i)}>☆</span>
    ))
}
     <span className={styles.ratersQty}>{ratersQty}</span>    

        
    </div>
  )
})

export default RatingComponent