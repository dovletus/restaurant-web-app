import React, { useContext, useEffect, useState } from "react";
import { observer } from "mobx-react-lite";

import { MyContext } from "../../../App";
import ProductCard from "../ProductCard";
import styles from "./styles.module.css";
import { baseUrl } from "../../../config";

const ProductList = observer(() => {
  const { productStore, categoryStore } = useContext(MyContext);
  const list = productStore.List;
  const selectedCategory = categoryStore.SelectedItem;
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    //closure
    (async function () {
      setLoading(true);
      fetch(`${baseUrl}/products`)
        .then((res) => res.json())
        .then((data) => {
          // console.log("selected category", selectedCategory.id);
          if (selectedCategory.id === 0) {
            productStore.setList(data);
            return;
          }

          const filteredItems = data.filter(
            (item) => item.catId === selectedCategory.id
          );
          productStore.setList(filteredItems);
          // productStore.setSelected(filteredItems[0]);
        })
        .catch((err) => console.log("Error in fetching products", err))
        .finally(() => setLoading(false));
    })();
  }, [selectedCategory]);

  return (
    <div className={styles.root}>
      {/* <button onClick={openAddCategory}>Add category</button>
      {isVisible && <AddCategory close={closeAddCategory} />} */}

      <div className={styles.container}>
        {list.map((item) => (
          <ProductCard product={item} key={item.id} />
        ))}
      </div>
    </div>
  );
});

export default ProductList;
