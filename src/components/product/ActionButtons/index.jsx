import React, { useContext } from 'react'
import { faCartPlus, faCartShopping, faHeart} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { observer } from 'mobx-react-lite'

import { MyContext } from '../../../App'

import styles from './styles.module.css'
import { faHeart as Heart_Regular} from '@fortawesome/free-regular-svg-icons'

const ActionButtons=observer(({product}) =>{
const {likeStore,basketStore}=useContext(MyContext)

    const isLikeProduct = likeStore.isLikeProduct(product.id)
    const isBasketProduct = basketStore.isBasketProduct(product.id)
    
    const handleAddToLike = ()=>{
        if(isLikeProduct) {
            likeStore.removeFromLike(product.id)
            return;
        } 

        likeStore.addToLike(product)
    }

    const handleAddToBasket = ()=>{
        if(isBasketProduct) {
         basketStore.removeFromBasket(product.id)   
            return;
        }

        basketStore.addToBasket(product)
    }

  return (
    <div className={styles.root}>
                
        <span className={styles.btnHeart} onClick={handleAddToLike}>
            <FontAwesomeIcon icon={isLikeProduct ? Heart_Regular : faHeart} color={isLikeProduct ? 'red':'gold'} fontSize={15}/>
            
            </span>

        <span className={styles.btnBasket} onClick={handleAddToBasket}>
        <FontAwesomeIcon icon={isBasketProduct ? faCartShopping : faCartPlus} color={isBasketProduct ? 'gold':'green'} fontSize={15}/>
            
            </span>
        </div>
  )
})

export default  ActionButtons
