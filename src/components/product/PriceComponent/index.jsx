import React from 'react'

import styles from './styles.module.css'

export default function PriceComponent({value}) {
  return (
    <div className={styles.root}>
        {
            value.discount && 
            <>
                <span  className={styles.old_price}>{value.old_price}</span>
                <span  className={styles.discount}>{value.discount} %</span>
            </>
        }
        <span  className={styles.price}>{value.price}</span>
        
    </div>
  )
}
