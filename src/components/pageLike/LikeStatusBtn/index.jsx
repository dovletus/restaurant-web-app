import React, { useContext } from 'react' 
import { observer } from 'mobx-react-lite'

import { MyContext } from '../../../App'
import styles from './styles.module.css'
import { Link } from 'react-router-dom'
import { routes } from '../../../navigation/routes'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart } from '@fortawesome/free-regular-svg-icons'

const LikeStatusBtn=observer(()=> {
    const {likeStore}=useContext(MyContext)

    const likeLength = likeStore.LikeListLength


  return (
    <Link to={routes.like} className={styles.root}>
    <FontAwesomeIcon icon={faHeart} fontSize={10}/>
      <span className={styles.count}>{likeLength}</span> 
    </Link>
  )
})

export default LikeStatusBtn