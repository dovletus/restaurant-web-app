import React, { useContext, useState } from 'react'
import ReactDOM from 'react-dom';
import { Formik, Field, Form } from 'formik';
import { MyContext } from '../../App'

import styles from './styles.module.scss'

export default function LoginForm() {
  const {userStore} = useContext(MyContext)
  
const user = '123'
const pass = '123'

  const handleSubmit = ({username,password})=>{
    if(username=== user && password === pass){
      userStore.login({id:1, username:username, level:'ADMIN', token: 'abc123'})
      userStore.setLoginFormVisible(false)
    } else {
      alert('Your password or username is incorrect')
    }
    
  }

  return (
    <div className={styles.root}>
      <h2>Login</h2>
    <Formik
    initialValues={{
      username: '',
      password: ''      
    }}
    onSubmit={(values)=>handleSubmit(values)}
    >
      <Form>
        <label htmlFor="username">Username:</label>
        <Field id="username" name="username" placeholder="adynyz" />

        <label htmlFor="password">Password:</label>
        <Field id="password" type='password' name="password" placeholder="acarsoz"/>

        <button type="submit">Login</button>

      </Form>
    </Formik>
    </div>
  )
}

{/* <Form>
        <label htmlFor="firstName">Username:</label>
        <Field id="firstName" onChange={e=> setUserName(e.target.value)} value={userName}/>

        <label htmlFor="lastName">Password:</label>
        <Field id="lastName" type='password' onChange={e=> setPassword(e.target.value)} value={password}/>

        <button type="submit">Login</button>

      </Form> */}