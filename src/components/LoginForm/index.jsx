import React, { useContext, useEffect, useState } from 'react'
import ReactDOM from 'react-dom';
import { Formik, Form, Field, ErrorMessage } from 'formik';
// import * as Yup from 'yup'
import {object, string, number } from 'yup'

import { MyContext } from '../../App'

import styles from './styles.module.scss'
import { userStoreKeys } from '../../store/UserStore';
import { host } from '../../http';
import { dlog } from '../../utilities/log';
import { observer } from 'mobx-react-lite';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClose } from '@fortawesome/free-solid-svg-icons';

const LoginForm=observer(()=> {
  const {userStore} = useContext(MyContext)

  const user = userStore.getKey(userStoreKeys.user)
  const hasFetched = userStore.getKey(userStoreKeys.hasFetched)


  const [loading,setLoading]=useState(false)
  
  // const handleSubmit = ({username,password})=>{
  //   if(username=== user && password === pass){
  //     userStore.login({id:1, username:username, level:'ADMIN', token: 'abc123'})
  //     userStore.setLoginFormVisible(false)
  //   } else {
  //     alert('Your password or username is incorrect')
  //   }
    
  // }

  const initialValues = { email: "", password: "" };

  const validationSchema = object({
    email: string().email("Email adresiňizi dogry ýazyň").required(),
    password: string().required(),
  });

  const onSubmit = ({email,password}) => {
    if(email===user.email && password === user.password){
      userStore.login({email, password, level:'ADMIN', token: 'abc123'})
      userStore.setKey(userStoreKeys.isFormVisible, false)
    } else {
      alert('Your password or username is incorrect')
    }
  };

  const handleClose = ()=>userStore.setKey(userStoreKeys.isFormVisible,false)

  useEffect(()=>{
    async function getUserData(){
      setLoading(true)
      host.get('/user')
      .then(({data})=>userStore.setKey(userStoreKeys.user, data))
      .catch(err=>dlog.error('fetching user data .. ',err))
      .finally(()=>setLoading(false))
    } 

    if(!hasFetched) getUserData()
  },[])


  return (
    <div className={styles.root}>
      <FontAwesomeIcon icon={faClose} onClick={handleClose} color='red' style={{textAlign:'right'}}/>
      <h2>Login</h2>
        <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
            {({ touched, errors }) => (
              <Form>
                <div>
                  <label htmlFor="email">Email</label>
                  <Field type="email" id="email" name="email" className={errors.email && touched.email ? "error" : null} />
                  <ErrorMessage name="email" component="div" className="error-message" />
                </div>
                <div>
                  <label htmlFor="password">Açarsöz</label>
                  <Field type="password" id="password" name="password" className={errors.password && touched.password ? "error" : null} />
                  <ErrorMessage name="password" component="div" className="error-message" />
                </div>
                <button type="submit">Tabşyr</button>
              </Form>
            )}
          </Formik>
    </div>
  )
}
)

export default LoginForm

{/* <Form>
        <label htmlFor="firstName">Username:</label>
        <Field id="firstName" onChange={e=> setUserName(e.target.value)} value={userName}/>

        <label htmlFor="lastName">Password:</label>
        <Field id="lastName" type='password' onChange={e=> setPassword(e.target.value)} value={password}/>

        <button type="submit">Login</button>

      </Form> */}