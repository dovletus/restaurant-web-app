import React, { useContext } from "react";
import { observer } from "mobx-react-lite";

import { MyContext } from "../../App";
import styles from "./styles.module.css";
import ProductDetailsCard from "../product/ProductDetailsCard";

const ProductDetails = observer(() => {
  const { productStore } = useContext(MyContext);
  const product = productStore.SelectedItem;

  return (
    <ProductDetailsCard product={product}/>
  );
});

export default ProductDetails;
