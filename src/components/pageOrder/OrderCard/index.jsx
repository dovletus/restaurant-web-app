import React, { useContext } from 'react'
import { useNavigate } from 'react-router-dom'
import { MyContext } from '../../../App'
import { routes } from '../../../navigation/routes'
import cn from 'classnames'
import styles from './styles.module.scss'


export default function OrderCard({order, index}) {
const {orderStore}=useContext(MyContext)
  const date = new Date(order.date).toLocaleString("tr-TR", { timeZone: "Asia/Ashgabat" })
  // const date = new Date(order.date).toUTCString().substring(0,25)


  const navigate = useNavigate()

  function handleShowOrder(){
    orderStore.setSelected(order)
    navigate(routes.orderDetails, {order})
  }

  return (
    <tr className={styles.row}>
       <td className={styles.center}>{index}</td>
        <td className={cn(styles.center, styles.dateCol)}>{date}</td>
        <td className={styles.center}>{order.totalSum}</td>
        <td className={cn(styles.center, styles.action)} onClick={handleShowOrder}>show</td>
    </tr>
  )
}
