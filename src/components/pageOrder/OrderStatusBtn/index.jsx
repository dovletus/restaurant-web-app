import { faTruckFast } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { MyContext } from '../../../App'
import { routes } from '../../../navigation/routes'
import styles from './styles.module.scss'

const OrderStatusBtn=observer(()=> {
    const {orderStore} = useContext(MyContext)

    const orderListLength = orderStore.OrderListLength


  return (
    <Link to={routes.order} className={styles.root} >
        <FontAwesomeIcon icon={faTruckFast} fontSize={10}/>
        <span className={styles.count}>{orderListLength}</span>
    </Link>
  )
})

export default OrderStatusBtn