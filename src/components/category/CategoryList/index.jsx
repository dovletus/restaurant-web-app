import React, { useContext, useEffect, useState } from "react";
import { observer } from "mobx-react-lite";

import { MyContext } from "../../../App";
import CategoryCard from "../CategoryCard";
import AddCategory from "../AddCategory";
import styles from "./styles.module.css";
import { baseUrl } from "../../../config";
import { fetchlist } from "../../../http/fetchList";

const CategoryList = observer(() => {
  const { categoryStore } = useContext(MyContext);
  const list = categoryStore.List;
  const [loading, setLoading] = useState(false);

  const [isVisible, setIsVisible] = useState(false);
  const openAddCategory = () => setIsVisible(true);
  const closeAddCategory = () => setIsVisible(false);

  useEffect(() => {
    // //closure
    // (async function () {
    //   setLoading(true);
    //   fetch(`${baseUrl}/categories`)
    //     .then((res) => res.json())
    //     .then((data) => {
    //       console.log("categories", data);
    //       categoryStore.setList(data);
    //       // categoryStore.setSelected(data[0]);
    //     })
    //     .catch((err) => console.log("Error in fetching categories", err))
    //     .finally(() => setLoading(false));
    // })();

    setLoading(true);
    fetchlist('/categories').then(data=>{
      categoryStore.setList(data);
      setLoading(false)
    })
  }, []);

  return (
    <div className={styles.root}>
      {/* <button onClick={openAddCategory}>Add category</button> */}
      {/* {isVisible && <AddCategory close={closeAddCategory} />} */}

      <div className={styles.categoryList}>
        {list.map((item) => (
          <CategoryCard category={item} key={item.id} />
        ))}
      </div>
    </div>
  );
});

export default CategoryList;
