import { observer } from "mobx-react-lite";
import React, { useContext } from "react";
import { useMemo } from "react";
import { MyContext } from "../../../App";
import styles from "./styles.module.css";

const CategoryCard = observer(({ category }) => {
  const { categoryStore } = useContext(MyContext);
  const selectedCategory = categoryStore.SelectedItem;
  const isSelected = category.id === selectedCategory.id;

  const bgColor = useMemo(
    () => ({
      backgroundColor: isSelected ? "white" : "white",
    }),
    [isSelected]
  );

  const handleClick = () => categoryStore.setSelected(category);

  return (
    <div className={styles.root} style={bgColor} onClick={handleClick}>
      <div className={styles.imgContainer}>
        <img
          // src={{uri: `https://server/images/${category.img}`}}
          // src={imgSrc}
          src={`http://localhost:4000/images/${category.img}`}
          className={styles.img}
          alt="slice of pizza "
        />
      </div>
      <span className={styles.title}>{category.title}</span>
    </div>
  );
});

export default CategoryCard;
