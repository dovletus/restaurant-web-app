import React, { useContext, useEffect, useRef } from "react";
import { MyContext } from "../../../App";

export default function AddCategory({ close }) {
  const { categoryStore } = useContext(MyContext);
  const inputRef = useRef();

  function handleAddCategory() {
    const newCategory = {
      id: new Date().getTime(),
      title: inputRef.current.value,
      img: "category.jpg",
    };

    categoryStore.addItem(newCategory);
    inputRef.current.select();
  }

  useEffect(() => {
    inputRef.current.focus();
  }, []);
  return (
    <div onClick={close}>
      <div>
        <span>Add category</span>
        <div className="form">
          <input ref={inputRef} type="text" placeholder="title" />
          <button onClick={handleAddCategory}>Add</button>
        </div>
      </div>
    </div>
  );
}
