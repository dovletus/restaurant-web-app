import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import { faHeart } from '@fortawesome/free-regular-svg-icons'
import { routes } from '../../../navigation/routes'
import { faCartShopping, faTruckFast } from '@fortawesome/free-solid-svg-icons'
import { MyContext } from '../../../App'
import SelectedButton from '../SelectedButton'

import styles from "./styles.module.scss"

const buttons = [
    {route: routes.like, icon: faHeart },
    {route: routes.basket, icon: faCartShopping },
    {route: routes.order, icon: faTruckFast }
]

const UserNavBar = observer(()=> {
    const {orderStore, likeStore, basketStore} = useContext(MyContext)

    const likeLength = likeStore.LikeListLength
    const basketListLength = basketStore.BasketListLength
    const orderListLength = orderStore.OrderListLength

    const lengths =[
        likeLength, basketListLength, orderListLength
    ]

  return (
    <div className={styles.root}>
    {
        buttons.map((item, i)=> (
            <SelectedButton route={item.route} icon={item.icon} key={i} 
            length={lengths[i]}
            />
        ))
    }
    </div>
  )
})

export default  UserNavBar