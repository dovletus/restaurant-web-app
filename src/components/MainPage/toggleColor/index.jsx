import React, { useContext, useMemo } from "react";
import { observer } from "mobx-react-lite";

import { MyContext } from "../../../App";

const ToggleColor = observer(() => {
  const { themeStore } = useContext(MyContext);
  const { bgColor, textColor } = themeStore.themeColors;

  const bgStyle = useMemo(
    () => ({
      display: "flex",
      flexDirection: "column",
      backgroundColor: bgColor,
    }),
    [bgColor]
  );

  const textStyle = useMemo(
    () => ({ fontSize: 20, color: textColor }),
    [textColor]
  );

  function handleToggle() {
    themeStore.toggleTheme();
  }

  return (
    <div style={bgStyle}>
      <button onClick={handleToggle}>Change</button>
    </div>
  );
});

export default ToggleColor;
