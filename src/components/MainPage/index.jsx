import React, { useContext, useMemo } from "react";
import { observer } from "mobx-react-lite";

import { MyContext } from "../../App";
import CategoryList from "../category/CategoryList";
import ProductList from "../product/ProductList";
import ProductDetails from "../details";
import { LikeStatusBtn } from "../pageLike";
import { BasketStatusBtn } from "../pageBasket";
import styles from './styles.module.css'
import OrderStatusBtn from "../pageOrder/OrderStatusBtn";
import { AdminNavbar } from "../../pages/AdminPage/components";
import UserNavBar from "./UserNavBar";

const MainPage = observer(() => {
  const { themeStore, userStore } = useContext(MyContext);
  const { bgColor, textColor } = themeStore.themeColors;

  const bgStyle = useMemo(
    () => ({
      display: "flex",
      flexDirection: "column",
      backgroundColor: bgColor,
    }),
    [bgColor]
  );


  return (
    <div style={bgStyle}>
        <AdminNavbar />
       
      <div className={styles.statusBtns}>
        {/* <LikeStatusBtn />
        <BasketStatusBtn/>
        <OrderStatusBtn /> */}
        <UserNavBar/>
      </div>

      <CategoryList />
      <ProductList />
      {/* <ProductDetails /> */}
    </div>
  );
});

export default MainPage;
