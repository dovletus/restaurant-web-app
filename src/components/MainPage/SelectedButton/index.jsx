import React, { useContext } from "react";
import { observer } from "mobx-react-lite";

import { MyContext } from "../../../App";
import styles from "./styles.module.css";
import { Link } from "react-router-dom";
import { routes } from "../../../navigation/routes";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCartShopping } from "@fortawesome/free-solid-svg-icons";

const SelectedButton = observer(({ route, icon, length }) => {
  return (
    <Link to={route} className={styles.root}>
      <FontAwesomeIcon icon={icon} fontSize={10} />
      <span className={styles.count}>{length}</span>
    </Link>
  );
});

export default SelectedButton;
