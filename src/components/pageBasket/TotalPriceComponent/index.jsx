import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'
import { MyContext } from '../../../App'
import styles from './styles.module.scss'
import PriceLabel from './PriceLabel'
import { host } from '../../../http'
import { minTotalPrice, shippingCost } from '../../../config'
import { dlog } from '../../../utilities/log'

const TotalPriceComponent=observer(()=> {
  const {basketStore}=useContext(MyContext)

  const totalPrice = basketStore.TotalPrice;
  const shipping = shippingCost
  const MinTotalPrice = minTotalPrice
  const totalSum = totalPrice+shipping

  const isMinTotalPriceVisible = MinTotalPrice>totalSum

  function handlePlaceOrder(){
    const basketList = basketStore.List

    const newOrder = {
      productList:basketList,
      totalSum,
      date: new Date()  
    }

    host.post('/orders',newOrder)
    .then(({data})=>{
      basketStore.emptyBasket()
    })
    .catch(err=>dlog.error('Error in placing order..',err))
  }

  return (
    <div className={styles.root}>      
      <PriceLabel title= 'Total Price:' value={totalPrice}/>
      {isMinTotalPriceVisible&&
        <PriceLabel title='Minimum:' value={MinTotalPrice} style={{color:'red'}}/>
      }
      <PriceLabel title='Shipping:' value={shipping}/>
      <PriceLabel title='Total sum:' value={totalSum}/>
      <button onClick={handlePlaceOrder}>Place order</button>
      </div>
  )
})

export default TotalPriceComponent