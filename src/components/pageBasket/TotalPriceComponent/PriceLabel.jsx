import React from 'react'
import styles from './styles.module.scss'
import cn from 'classnames'

export default function PriceLabel({title,value,style=null}) {

  return (
    <div className={styles.labelContainer }>
        <span className={cn(styles.labelTitle, {[styles.redText]:Boolean(style)})}>{title}</span>
        <span className={styles.labelPrice}>{value}</span>
    </div>
  )
}
