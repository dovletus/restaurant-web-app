import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import cn from 'classnames'

import styles from './styles.module.scss'
import { MyContext } from '../../../App'

const ChangeQuantity=observer(({product})=> {
const {basketStore}=useContext(MyContext)

    function handleChange(change){
        //if qty===1 return;
        if(product.qty===1 && change<1) return;
        
        product.qty +=change
        product.totalPrice =product.qty*product.price

      basketStore.updateBasketProduct({...product})
    }

  return (
    <div className={styles.root}>
        <span className={cn(styles.btnMinus,styles.btn)}
         onClick={()=>handleChange(-1)}>-</span>
        <span className={styles.textQty}>{product.qty}</span>
        <span className={cn(styles.btnPlus,styles.btn)}
         onClick={()=>handleChange(1)}>+</span>

    </div>
  )
})

export default  ChangeQuantity