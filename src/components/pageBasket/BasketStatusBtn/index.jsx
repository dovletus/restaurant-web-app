import React, { useContext } from 'react' 
import { observer } from 'mobx-react-lite'

import { MyContext } from '../../../App'
import styles from './styles.module.css'
import { Link } from 'react-router-dom'
import { routes } from '../../../navigation/routes'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartShopping } from '@fortawesome/free-solid-svg-icons'

const BasketStatusBtn=observer(()=> {
    const {basketStore}=useContext(MyContext)

    const basketListLength = basketStore.BasketListLength

    const handleNavigate = ()=>{}

  return (
    <Link to={routes.basket} className={styles.root} onClick={handleNavigate}>
      <FontAwesomeIcon icon={faCartShopping} fontSize={10}/>
      <span className={styles.count}>{basketListLength}</span> 
       </Link>
  )
})

export default BasketStatusBtn