export const routes={
    main:"/",
    like:"/like",
    basket:"/basket",
    order:"/orders",
    orderDetails:"/orderDetails",
    productDetails:"/details",
    profile:"/profile",
    //admin routes
    admin:'/admin',
    adminCategories:'/adminCategories',
    adminProducts:'/adminProducts',

}