import { observer } from "mobx-react-lite";
import React, { useContext } from "react";
import { createBrowserRouter as Router, Navigate, RouterProvider } from "react-router-dom";
import { MyContext } from "../App";
import { AdminCategoriesPage, AdminPage, AdminProductsPage, BasketPage, LikePage, MainPage, OrderDetails, OrderPage, ProductDetailsPage } from "../pages";
import { routes } from "./routes";

const router = Router([
    {
        path: routes.main,
        element: <MainPage />
    },
    {
        path: routes.like,
        element: <LikePage />
    },
    {
        path: routes.basket,
        element: <BasketPage />
    },
    {
        path: routes.order,
        element: <OrderPage />
    },
    {
        path: routes.orderDetails,
        element: <OrderDetails />
    },
    {
        path: routes.productDetails,
        element: <ProductDetailsPage />
    },
    {
        path: '/*',
        element: <Navigate to={routes.main} /> 
    }
])

const adminRouter = Router([
    {
        path: routes.admin,
        element: <AdminPage />
    },
    {
        path: routes.adminCategories,
        element: <AdminCategoriesPage />
    },
    {
        path: routes.adminProducts,
        element: <AdminProductsPage />
    },
    {
        path: routes.main,
        element: <MainPage />
    },
    {
        path: routes.like,
        element: <LikePage />
    },
    {
        path: routes.basket,
        element: <BasketPage />
    },
    {
        path: routes.order,
        element: <OrderPage />
    },
    {
        path: routes.orderDetails,
        element: <OrderDetails />
    },
    {
        path: routes.productDetails,
        element: <ProductDetailsPage />
    },
    {
        path: '/*',
        element: <Navigate to={routes.main} /> 
    }
])

const Navigation=observer(()=>{
    const {userStore}=useContext(MyContext)

    const isAuth = userStore.IsAuth

    return <RouterProvider router={isAuth ? adminRouter : router}/>
})

export default Navigation