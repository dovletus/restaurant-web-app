import React, { useContext } from "react";
import { createBrowserRouter as Router } from "react-router-dom";
import { RouterProvider } from "react-router";
import ProductList from "./components/ProductList";
import Sebet from "./components/Sebet";
import OrderList from "./components/OrderList";
import { MyContext } from "./Context";
import ErrorElement from "./components/atoms/ErrorElement";

export const pathProductList = "/";
export const pathSebet = "/sebet";
export const pathOrders = "/orders";
export const pathOther = "*";

const navAuth = Router([
  {
    path: pathProductList,
    element: <ProductList />,
    // errorElement:<ErrorElement/>
  },
  {
    path: pathSebet,
    element: <Sebet />,
  },
  {
    path: pathOrders,
    element: <OrderList />,
  },
  {
    path: pathOther,
    element: <ErrorElement />,
  },
]);

const nav = Router([
  {
    path: pathProductList,
    element: <ProductList />,
  },
  {
    path: pathSebet,
    element: <Sebet />,
  },
  {
    path: pathOrders,
    element: <OrderList />,
  },
  {
    path: pathOther,
    element: <ErrorElement />,
  },
]);

export default function RouterComponent() {
  console.log('render Router')
  const ctx = useContext(MyContext);

  if (ctx.token) return <RouterProvider router={navAuth} />;

  return <RouterProvider router={nav} />;
}
