
import axios from "axios"
import { baseUrl } from "../config"

const host = axios.create({
    baseURL:baseUrl,
    timeout:3000
})


const authhost = () => {
    let userData = localStorage.getItem('_UserStore')
    userData = JSON.parse(userData)

return axios.create({
    baseURL:baseUrl,
    timeout:3000,
    headers:{Authorization:`Bearer ${userData.token}`}
})
}

export {host,authhost}
