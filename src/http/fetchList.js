import {host} from '.'
import { dlog } from '../utilities/log'

export async function fetchlist(url){
    return host.get(url)
    .then(res=>res.data)
    .catch(err=>{
        dlog.error(`fetching ${url}`,err);
        return null;
    })
    .finally(()=>dlog.finally(`done fetchList`))
}