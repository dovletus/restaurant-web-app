import React, {useState} from 'react'

export function useNumber(initialValue=0) {
const [number,setNumber]=useState(initialValue)

function increment(by){
    setNumber(prev=>Number.isInteger(by) ? prev+by : prev + 1 )
}

function decrement(by){
    setNumber(prev=>Number.isInteger(by) ? prev-by : prev - 1 )
}

function setNumberVal(val){
    setNumber(val)
}

  return [number,setNumberVal, increment, decrement]
}
