import React, { createContext, useContext } from 'react'
import { useNumber } from './number'

const NumberCtx = createContext() 

export const useNumberCtx = ()=>useContext(NumberCtx)

export default function NumberContextProvider({children}) {
    const [number, setNumberVal, increment, decrement]=useNumber(0)

  return (
    <NumberCtx.Provider value={{number, setNumberVal, increment, decrement}}>
        {children}
    </NumberCtx.Provider>
  )
}
