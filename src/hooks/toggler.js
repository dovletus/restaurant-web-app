import React , {useState}from 'react'

export function useToggle(initialValue=false) {
    const [value, setValue]=useState(initialValue)
    console.log('value in hook',value)

    function toggle(){
        setValue(prev=>!prev)
    }

  return [value, toggle]
}
